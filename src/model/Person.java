package model;

public class Person {
    private String firstname;
    private String lastname;
    private String gender;
    private String contact;
    private String email;

    // getter & setter
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getContact() {
        return contact;
    }
    public void setContact(String contact) {
        this.contact = contact;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    

    // contructor
    // không có tham số
    public Person() {
    }
    // có tham số
    public Person(String firstname, String lastname, String gender, String contact, String email) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.contact = contact;
        this.email = email;
    }
    
    
}
