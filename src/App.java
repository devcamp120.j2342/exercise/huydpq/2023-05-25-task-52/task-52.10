import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person();
        Person person2 = new Person("Devcamp", "user", "male", "090", "devcamp.com.vn");

        System.out.println(person1.toString());
        System.out.println(person2.toString());

        System.out.println(person1.getFirstname());
        System.out.println(person2.getFirstname());

        person1.setFirstname("Update");
        System.out.println(person1.getFirstname());
        System.out.println(person2.getFirstname());

    }
}
